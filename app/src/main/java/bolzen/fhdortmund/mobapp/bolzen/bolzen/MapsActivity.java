package bolzen.fhdortmund.mobapp.bolzen.bolzen;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQueryDataEventListener;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final LatLng DEFAULT_LATLNG = new LatLng(51.493,7.417);
    public static final double SEARCHRADIUS = 50;

    private GoogleMap mMap;
    Button filterBT;
    static String aktPlatz;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Sportplatz");
    GeoFire geofire = new GeoFire(database.getReference("Geolocation"));
    ArrayList<Sportplatz> plaetze = new ArrayList<Sportplatz>();
    Marker hereMarker;
    Collection<Marker> platzMarkers = new ArrayList<Marker>();



    int filter=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.location_map);
        mapFragment.getMapAsync(this);

        filterBT = (Button) findViewById(R.id.filterBT);
        filterBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (filter==0)
                    Toast.makeText(MapsActivity.this, "Alle Sportarten",Toast.LENGTH_SHORT).show();
                if (filter==1)
                    Toast.makeText(MapsActivity.this, "Basketball",Toast.LENGTH_SHORT).show();
                if (filter==2)
                    Toast.makeText(MapsActivity.this, "Fußball",Toast.LENGTH_SHORT).show();



                mMap.clear();
                //finish();
                //startActivity(getIntent());
                for (Sportplatz spp: plaetze) {
                    if (filter>0) {
                        if (spp.getSportarten().contains("" + filter)) {
                            final LatLng platz = new LatLng(spp.getX(), spp.getY());
                            mMap.addMarker(new MarkerOptions().position(platz).title(spp.getName()));
                        }
                    }
                    else {
                        final LatLng platz = new LatLng(spp.getX(), spp.getY());
                        mMap.addMarker(new MarkerOptions().position(platz).title(spp.getName()));
                    }
                }

                if (filter<2)
                    filter++;
                else filter=0;

            }
        });

    }

    @Override
    protected void onResume() {
            super.onResume();
        Toast.makeText(MapsActivity.this, "Resume",Toast.LENGTH_SHORT).show();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        hereMarker = mMap.addMarker(new MarkerOptions().position(DEFAULT_LATLNG).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(hereMarker.getPosition(),15.0f));
        populateMap();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if(marker.equals(hereMarker)){
                    Intent intent = new Intent(MapsActivity.this,AddPlaceActivity.class);
                    intent.putExtra("latitude",marker.getPosition().latitude);
                    intent.putExtra("longitude",marker.getPosition().longitude);
                    startActivity(intent);
                } else {
                    aktPlatz = marker.getTitle();
                    startActivity(new Intent(MapsActivity.this, ListActivity.class));
                }
                return true;
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                plaetze.clear();
                for (Marker m:platzMarkers){
                    m.remove();
                }
                platzMarkers.clear();
                hereMarker.setPosition(latLng);
                geofire.queryAtLocation(new GeoLocation(latLng.latitude,latLng.longitude),SEARCHRADIUS).addGeoQueryEventListener(new GeoQueryEventListener() {
                    @Override
                    public void onKeyEntered(String key, GeoLocation location) {
                        myRef.child(key).addListenerForSingleValueEvent(AddToMapVEL);
                    }

                    @Override
                    public void onKeyExited(String key) {

                    }

                    @Override
                    public void onKeyMoved(String key, GeoLocation location) {

                    }

                    @Override
                    public void onGeoQueryReady() {

                    }

                    @Override
                    public void onGeoQueryError(DatabaseError error) {

                    }
                });
                Toast.makeText(MapsActivity.this,"Zeige Plätze in "+Double.toString(SEARCHRADIUS)+"km. Erneut tippen zum Hinzufügen.",Toast.LENGTH_LONG).show();
            }
        });
    }
    void populateMap(){
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                for (DataSnapshot child : children) {
                    processResult(child);
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Toast.makeText(MapsActivity.this,"Error DB",Toast.LENGTH_LONG).show();
            }
        });
    }
    public void processResult(DataSnapshot data){
        Sportplatz sportplatz = data.getValue(Sportplatz.class);
        plaetze.add(sportplatz);
        final LatLng platz = new LatLng(sportplatz.getX(), sportplatz.getY());
        platzMarkers.add(mMap.addMarker(new MarkerOptions().position(platz).title(sportplatz.getName())));
    }
    ValueEventListener AddToMapVEL = new  ValueEventListener() {

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            processResult(dataSnapshot);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
