package bolzen.fhdortmund.mobapp.bolzen.bolzen;

public class Anmeldung {
    String sportplatzname;
    String nickname;
    String datum;
    String msg;

    public String getSportplatzname() {
        return sportplatzname;
    }

    public void setSportplatzname(String sportplatzname) {
        this.sportplatzname = sportplatzname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Anmeldung() {

    }

    public Anmeldung(String sportplatzname, String nickname, String datum, String msg) {

        this.sportplatzname = sportplatzname;
        this.nickname = nickname;
        this.datum = datum;
        this.msg = msg;
    }
}
