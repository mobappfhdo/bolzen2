package bolzen.fhdortmund.mobapp.bolzen.bolzen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddPlaceActivity extends AppCompatActivity {

    private static final String TAG = AddPlaceActivity.class.getName();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Sportplatz");
    GeoFire geofire = new GeoFire(database.getReference("Geolocation"));



    EditText etxName;
    CheckBox ckSportart1;
    CheckBox ckSportart2;
    EditText etxLatitude;
    EditText etxLongitude;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_place);

        etxName = findViewById(R.id.etxName);
        ckSportart1 = findViewById(R.id.ckSportart1);
        ckSportart2 = findViewById(R.id.ckSportart2);
        etxLatitude = findViewById(R.id.etxLatitude);
        etxLongitude = findViewById(R.id.etxLongitude);
        btnSave = findViewById(R.id.btnSave);

        Intent intent = getIntent();
        if(intent!=null){
            etxLatitude.setText(
                    Double.toString(intent.getDoubleExtra("latitude",0)));
            etxLongitude.setText(
                    Double.toString(intent.getDoubleExtra("longitude",0)));
        }
        btnSave.setOnClickListener(btnSaveClicked);
    }

    View.OnClickListener btnSaveClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sportplatz sportplatz = new Sportplatz();
            try {
                String name = etxName.getText().toString();
                Boolean sp1 = ckSportart1.isChecked();
                Boolean sp2 = ckSportart2.isChecked();
                float latitude = Float.parseFloat(etxLatitude.getText().toString());
                float longitude = Float.parseFloat(etxLongitude.getText().toString());
                String sportarten = "";
                if(sp1){
                    sportarten += "1,";
                }
                if(sp2){
                    sportarten += "2,";
                }
                if(name.isEmpty()){
                    return;
                }

                sportplatz.setName(name);
                sportplatz.setX(latitude);
                sportplatz.setY(longitude);
                sportplatz.setSportarten(sportarten);
            }catch(NumberFormatException e){
                Log.e(TAG, "onClick: ",e);
                return;
            }
            DatabaseReference newEntry = myRef.push();

            newEntry.setValue(sportplatz);
            geofire.setLocation(newEntry.getKey(),new GeoLocation(sportplatz.getX(), sportplatz.getY()),new GeoFire.CompletionListener() {
                @Override
                public void onComplete(String key, DatabaseError error) {
                    Toast.makeText(getApplicationContext(), "Done ", Toast.LENGTH_LONG).show();
                }
            });
            finish();
        }
    };
}
