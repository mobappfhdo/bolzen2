package bolzen.fhdortmund.mobapp.bolzen.bolzen;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;


public class ListActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();
    //ArrayList<Anmeldung> liste = new ArrayList<Anmeldung>();
    ArrayList<String> liste = new ArrayList<String>();
    ListView lv;
    ArrayAdapter<String> adapter;
    Button addBT;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        addBT=(Button)findViewById(R.id.addBT);



        addBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   add();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Toast.makeText(ListActivity.this,MapsActivity.aktPlatz,Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        super.onStart();

        addBT=(Button)findViewById(R.id.addBT);

        addBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  add();
            }
        });


       // writeNewAnmeldung("sportplatz2","testuser","03/04/12","bin heute abend hier");
        //writeNewAnmeldung("sportplatz2","testuser2","03/04/12","bin heute moorgen hier");


        myRef = database.getReference("Anmeldung");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                liste.clear();
                for (DataSnapshot child : children) {
                    Anmeldung anmeldung = child.getValue(Anmeldung.class);
                    if (anmeldung.getSportplatzname().equals(MapsActivity.aktPlatz))
                        liste.add(anmeldung.getSportplatzname()+" "+anmeldung.getNickname()+" "+anmeldung.getDatum()+" "+anmeldung.getMsg());
                }

                setContentView(R.layout.activity_list);
                lv = (ListView)findViewById(R.id.platzliste);
                adapter = new ArrayAdapter<String>(ListActivity.this,android.R.layout.simple_list_item_1, liste);
               lv.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Toast.makeText(ListActivity.this,"Error",Toast.LENGTH_LONG).show();
            }
        });

    }





    public void add(View v) {
        startActivity(new Intent(ListActivity.this, DatePickerActivity.class));
    }
}
