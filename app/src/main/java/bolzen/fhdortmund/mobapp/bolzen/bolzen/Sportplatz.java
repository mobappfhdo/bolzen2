package bolzen.fhdortmund.mobapp.bolzen.bolzen;

public class Sportplatz {
    Float x;
    Float y;
    String name;
    String sportarten;

    public Sportplatz() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Sportplatz(Float x, Float y,String name, String sportarten) {
        this.x=x;
        this.y=y;
        this.name=name;
        this.sportarten=sportarten;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public void setY(Float y) {
        this.y = y;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSportarten(String sportarten) {
        this.sportarten = sportarten;
    }

    public Float getX() {

        return x;
    }

    public Float getY() {
        return y;
    }

    public String getName() {
        return name;
    }

    public String getSportarten() {
        return sportarten;
    }
}
