package bolzen.fhdortmund.mobapp.bolzen.bolzen;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.*;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private Button loginBT;
    private Button regBT;
    private EditText emailTF;
    private EditText passTF;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth=FirebaseAuth.getInstance();

        loginBT=(Button)findViewById(R.id.loginBT);
        emailTF=(EditText)findViewById(R.id.emailTF);
        passTF=(EditText)findViewById(R.id.passTF);
        regBT=(Button)findViewById(R.id.regBT);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
           @Override
           public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
               if (firebaseAuth.getCurrentUser() != null) {
                   //logged in..
                    startActivity(new Intent(MainActivity.this, MapsActivity.class));
               }
           }
        };

        loginBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signin();
            }
        });

        regBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reg();
            }
        });
        ;
    }

    public void reg() {
        String email = emailTF.getText().toString();
        String pass= passTF.getText().toString();
        //Toast.makeText(MainActivity.this,email,Toast.LENGTH_LONG).show();
        //Toast.makeText(MainActivity.this,pass,Toast.LENGTH_LONG).show();
        mAuth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Registrierung erfolgreich.",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Registrierung fehlgeschlagen.",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void signin() {
        String email = emailTF.getText().toString();
        String pass= passTF.getText().toString();
        //Toast.makeText(MainActivity.this,email,Toast.LENGTH_LONG).show();
        //Toast.makeText(MainActivity.this,pass,Toast.LENGTH_LONG).show();
        mAuth.signInWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
            if (!task.isSuccessful()) {
                Toast.makeText(MainActivity.this,"Login fehlgeschlagen",Toast.LENGTH_LONG).show();
                System.out.println("onComplete: Failed=" + task.getException().getMessage());
            }
            else {
                Toast.makeText(MainActivity.this,"Login erfolgreich",Toast.LENGTH_LONG).show();
            }





        }
    });
    }

}
