package bolzen.fhdortmund.mobapp.bolzen.bolzen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.time.Month;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;

public class DatePickerActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();
    Button saveBT;
    FirebaseUser user;
    DatePicker dp;
    EditText detailsTF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_date_picker);
        user = FirebaseAuth.getInstance().getCurrentUser();
        setCurrentDate();
        saveBT = (Button) findViewById(R.id.saveBT);
        saveBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        });

    }

    public void save() {
        detailsTF=(EditText)findViewById(R.id.detailsTF);
        //Toast.makeText(DatePickerActivity.this,MapsActivity.aktPlatz + user.getEmail() + dp.getYear()+"-"+dp.getMonth()+"-"+ dp.getDayOfMonth() + detailsTF.getText(),Toast.LENGTH_LONG).show();
        writeNewAnmeldung(MapsActivity.aktPlatz, user.getEmail(), dp.getYear()+"-"+dp.getMonth()+"-"+ dp.getDayOfMonth(), detailsTF.getText().toString());
        Toast.makeText(DatePickerActivity.this,"Saved",Toast.LENGTH_LONG).show();
        startActivity(new Intent(DatePickerActivity.this,ListActivity.class));
    }

    private void writeNewAnmeldung(String sportplatzname, String nickname, String datum, String msg) {
        Anmeldung anmeldung = new Anmeldung(sportplatzname, nickname, datum, msg);
        myRef.child("Anmeldung").push().setValue(anmeldung);
    }

    public void setCurrentDate() {

    dp = (DatePicker) findViewById(R.id.simpleDatePicker);

 final Calendar calendar = Calendar.getInstance();

 int year = calendar.get(Calendar.YEAR);
 int month = calendar.get(Calendar.MONTH);
 int day = calendar.get(Calendar.DAY_OF_MONTH);

 dp.init(year, month, day, null);
    }
}
